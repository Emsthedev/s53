// Old practice in importing components
// import Navbar from 'react-bootstrap/NavBar';
// import Nav from 'react-bootstrap/Nav';
//
import { useState } from 'react' 
// New practice in importing components;
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function AppNavbar() {
	
	//getItem retrieves the data stored in the localStorage
	const [user, setUser] = useState(localStorage.getItem("email"))
	console.log(user);

	return (

		<Navbar bg="light" expand="lg">
		    <Container fluid>
		      <Navbar.Brand as={Link} to='/'>Zuitt</Navbar.Brand>
		      <Navbar.Toggle aria-controls="navbarScroll" />
		      <Navbar.Collapse id="navbarScroll">
		        <Nav className="ml-auto">
		          <Nav.Link as={Link} to='/'>Home</Nav.Link>
		          <Nav.Link as={Link} to='/courses'>Courses</Nav.Link>
				  
				  { (user !== null) ? 
				 	 	<Nav.Link as={Link} to='/logout'>Logout</Nav.Link>
					:
					<>
						<Nav.Link as={Link} to='/register'>Register</Nav.Link>
						<Nav.Link as={Link} to='/login'>Login</Nav.Link>
					</>
				  }
				

		        </Nav>
		      </Navbar.Collapse>
		    </Container>
		  </Navbar>
	)
};