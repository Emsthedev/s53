import { useState, useEffect} from 'react';
import {  Col, Card, Button } from 'react-bootstrap';

export default function CourseCard({courseProp}) {
    //is to check if data was successfully passed
    //console.log(props);
    // Every component receives information in a form  of an object
    //console.log(typeof props);
    //console.log({courseProp});

    const { name, description, price, _id } = courseProp;

    /* 
        Use the state hook for this component to be able to store its state.
        States are used to keep track of the information related to individual
            components/elements
            Syntax:
            const [getter, setter] = useState(initialGetterValue);
    */

    //
    //  const [count, setCount] = useState(0)
    //  //console.log(useState(0));
    // 
   
    //  function enroll(){
    //     setCount(count + 1)
    //     console.log('Enrollees ' +  count);

        
    //  }

       // state hook to store the state of enrollees
       let [count, setCount] = useState(0);
       let [seats, setSeats] = useState(10);
   
       
       //console.log(useState(0));
   
       function enroll(){
         if (seats > 0 ) {
           setCount(count + 1)
           console.log('Enrollees: ' + count)
           setSeats(seats - 1)
           console.log('Seats: ' + seats)
         } //else {
           //alert("No more seats available.")
        // }
       };

       useEffect(()=>{
            if (seats === 0) {
                alert("No more seats available.")
            }

       },[seats])
   

   


    return(
       
      
        <Col xs={12} md={4} >
            <Card className="cardCourses p-3" >
                <Card.Body >
                   
                    <Card.Header className="text-center card-header">{name}</Card.Header>
                    <Card.Subtitle className="mt-4 text-">Description:</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>{price}</Card.Text>
                    <Card.Text>Enrollees: {count}</Card.Text>
                    <Card.Text>Seats Available: {seats}</Card.Text>
                    <Button className='bg-primary' onClick={enroll}>Enroll</Button>
                   
                </Card.Body>
            </Card>
        </Col>

        
      
    )
};