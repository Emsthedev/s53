import { Navigate } from 'react-router-dom';

export default function Logout(){
    //clears the data in local storage when logged out
	localStorage.clear();

   

	return(
        // Redirect / Navigate the user back to the Login page
		<Navigate to="/login"/>
       
   

	)
};
