// import logo from './logo.svg';
//import { Fragment } from 'react';
import { Container } from 'react-bootstrap'
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';

//components
import AppNavbar from './components/AppNavbar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
//import CourseCard from './components/CourseCard'
//pages
import Courses from './pages/Courses';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login'
import Logout from './pages/Logout';
import PageNotFound from './pages/PageNotFound'
//stylesheet
import './App.css';


function App() {
  return (
    <Router>
     
      <AppNavbar/>
      <Container>
        <Routes>
           <Route path='/' element={ <Home/>} />
           <Route path='/courses' element={ <Courses/>} />
           <Route path='/register' element={ <Register/>} />
           <Route path='/login' element={ <Login/>} />
           <Route path='/logout' element={ <Logout/>} />
           <Route path='*' element={<PageNotFound/>}/>

           
        </Routes>
      </Container>
   
    </Router>
  );
}

export default App;
